package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import model.data_structures.GrafoNoDirigido;
import model.logic.MVCModelo;
import view.MVCView;

public class Controller 
{
	//Atributo
	public GrafoNoDirigido grafoNoDirigido;

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					// reporte la cantidad de vértices y arcos del grafo por consola.
					
					System.out.println("--------- \nCreando Grafo \n-------------------- ");
					
					
					modelo.cargarIntersecciones();
					modelo.leerArcos();					
					
					
					view.printMessage("El grafo se cargo correctamente \n---------------");
					view.printMessage("La cantidad de vertices en el grafo es:"+ modelo.darNumVertices()+"\n---------------" );
					view.printMessage("La cantidad de arcos en el grafo es:"+ modelo.darNumArcos()+"\n---------------");
					modelo.darNumVertices();
					break;

				case 2:
					
					
					System.out.println("--------- \nSe esta creando el archivo Json"+"\n---------------");
				try {
					modelo.crearElArchivoJson();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
				try {
					modelo.crearElArchivoJson();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
					System.out.println("Se creo correctamente el archivo Json"+"\n---------------");
					modelo.CargaElArchivoJson();			
					
					break;

				case 3:
					
					
					System.out.println("--------- \nSe empezo a cargar el grafo definido en el punto anterior");
					
					modelo.CargaElArchivoJson();
					
					view.printMessage("El grafo se cargo correctamente \n---------------");
					view.printMessage("La cantidad de vertices en el grafo es:"+ modelo.darNumVerticesGrafoJson()+"\n---------------" );
					view.printMessage("La cantidad de arcos en el grafo es:"+ modelo.darNumArcosGrafoJson()+"\n---------------");
					
					modelo.darNumVerticesGrafoJson();
					break;

				case 4:
					System.out.println("--------- \n Se comenzoaron a buscar las componentes conectadas del grafo"+"\n---------------");
					
					
					view.printMessage("La cantidad de componentes conectadas en el grafo es:"+ modelo.darNumComponentesConectadas() +"\n---------------" );
					
				
					
					break;

				case 5: 
					
					System.out.println("--------- \n Se esta Graficando con ayuda de Google Maps la parte del grafo resultante"+"\n---------------");
			
					modelo.graficar();
					
					break;	
					
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
