package model.data_structures;

public class Arco 
{

	private Vertice verticeIni;
	private Vertice verticeFinal;
	
	private double cost;

	public Arco( Vertice pverticeIni, Vertice pverticeFinal, double cost)
	{
		
		this.setVerticeIni(pverticeIni);
		this.setVerticeFinal(pverticeFinal);
		this.cost = cost;
		
	}
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Vertice getVerticeIni() {
		return verticeIni;
	}


	public void setVerticeIni(Vertice verticeIni) {
		this.verticeIni = verticeIni;
	}


	public Vertice getVerticeFinal() {
		return verticeFinal;
	}


	public void setVerticeFinal(Vertice verticeFinal) {
		this.verticeFinal = verticeFinal;
	}

}
