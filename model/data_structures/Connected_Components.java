package model.data_structures;

import java.util.Iterator;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;

public class Connected_Components 
{
	
	    // Atributo que dice si estan maracdos
		private boolean[] marked;   
	    
		//Id del componente
		private int[] id;          
	 
		//Numero de componentes conectados 
	    private int count;       

	    
	    
	    /**
	     * Saca las componentes conectadas de un grafo usando recursion
	     *
	     * @param G the undirected graph
	     */
	    public Connected_Components(GrafoNoDirigido G) 
	    {
	        marked = new boolean[G.DarNumeroDeVertices()];
	        id = new int[G.DarNumeroDeVertices()];
	        
	        
	        for (int v = 0; v < G.DarNumeroDeVertices(); v++) {
	            if (!marked[v]) {
	                dfs(G, v);
	                count++;
	            }
	        }
	    }
	    
	    /**
	     * Calcula el DFS de un grafo.
	     * @param G
	     * @param v
	     */
	    private void dfs(GrafoNoDirigido G, int v) {
	        marked[v] = true;
	        id[v] = count;
	        
	        Arco[] lista = G.arregloFijoarcos(v);
	        
	        for (int i = 0; i < lista.length; i++) 
	        {
	            if (!marked[i]) 
	            {
	                dfs(G, i);
	            }	
			}
	        
	
	    }

	    /**
	     * Retorna la cantidad de componentes conectadas en el grafo
	     */
	    public int count() 
	    {
	        return count;
	    }




}
