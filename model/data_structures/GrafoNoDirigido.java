package model.data_structures;

import java.util.Iterator;
import model.data_structures.*;
import java.util.NoSuchElementException;

import model.logic.Haversine;
import model.logic.Interseccion;

public class GrafoNoDirigido<K extends Comparable<K>, V> implements Iterable<K>
{
	
	private int sizeVertices;

	private int sizeEdges;


	HashTable<K, V> listaconVertices;




	/**
	 * Crea un grafo No dirigido de tamaño V vértices y sin arcos
	 * 
	 * @param V
	 */
	public GrafoNoDirigido(int tam ) 
	{

	
		this.sizeVertices = tam;
		this.sizeEdges = 0;

		listaconVertices = new HashTable<>(tam);
		
	}





	/**
	 * Número de vértices
	 * @return
	 */
	public int DarNumeroDeVertices()
	{
		return listaconVertices.size() ;
	}
	
	/**
	 * Número de arcos. Cada arco No dirigido debe contarse una única vez.
	 * @return
	 */
	public int DarNumeroDeEdges()
	{
		//TODO ver que si se muestre solo una vez 
		return sizeEdges;
	}



	/**
	 * Agrega un arco 
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param cost
	 */
	public void AgregarArco(K idVertexIni , K idVertexFin, double cost) //double cost 
	{
		
		sizeEdges++;
		
		Vertice primero = (Vertice) listaconVertices.get(idVertexIni);
	
		Vertice finall = (Vertice) listaconVertices.get(idVertexFin);
		
		
		primero.AgregarAdjacenciaALista(cost, primero, finall);
		
		
	}
	
	
	/**
	 * Obtener la información de un vértice. Si el vértice no existe retorna null.
	 * @param idVertex
	 * @return
	 */
	public V getInfoVertex(K idVertex)
	{
		V actual =  listaconVertices.get(idVertex);
		
		
		if(actual ==null) {
			return null;
		}
		else
		{
			return actual;
		}
	}
	
	/**
	 * Modificar la información del vértice idVertex
	 * @param idVertex
	 * @param infoVertex
	 */
	public void setInfoVertex(K idVertex, V infoVertex)
	{
		listaconVertices.setinfoVertex(idVertex, infoVertex);		
	}
	

	/**
	 * Obtener el costo de un arco, si el arco no existe, retorna -1
	 * @param idVertexIni
	 * @param idVertexFin
	 * @return
	 */
	public double getCostArc(K idVertexIni,K idVertexFin)
	{
		double ressp =0;
		
		Vertice resp = (Vertice)listaconVertices.get(idVertexIni);
		
		Iterator<Arco> lista = resp.darlistaAdjacencias();
		
		while(lista.next()!=null)
		{
			if (lista.next().getVerticeFinal().equals(idVertexFin) )
			{
				ressp = lista.next().getCost();
			}
			else 
			{
				ressp=-1;				
			}
		}
		return ressp;
		
	}
	
	/**
	 * Modificar el costo del arco entre los vértices idVertexIni e idVertexFin
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param cost
	 */
	public void setCostArc(K idVertexIni, K idVertexFin, double cost)
	{
		
		
		Vertice resp = (Vertice) listaconVertices.get(idVertexIni);
		
		Iterator<Arco> lista = resp.darlistaAdjacencias();
		
		
		while(lista.next()!=null)
		{
			Arco inicial= lista.next();
			
			if (inicial.getVerticeFinal().equals(idVertexFin) )
			{
				inicial.setCost(cost); ;
			}
			
		}
		
		
	}
	

	
	/**
	 * Adiciona un vértice con un Id único. El vértice tiene la información InfoVertex.
	 * @param idVertex
	 * @param infoVertex
	 */
	public void agregarVertice(K idVertex, V infoVertex) {
		
		if(listaconVertices.get(idVertex)==null)
		{
			listaconVertices.put(idVertex, infoVertex);
		}
		

	}

	
	/**
	 * 
	 * Retorna los identificadores de los vértices adyacentes a idVertex
	 * @param v
	 * @return
	 */
	public Iterator<Arco> arcosConectadosaK (K idVertex)
	{
		 Vertice encontrado = (Vertice) listaconVertices.get(idVertex);
		
		 
		return ((Iterator<Arco>) encontrado.darlistaAdjacencias());
	}

	
	
	public Arco[] arregloFijoarcos(K idVertex)
	{
		Vertice encontrado = (Vertice) listaconVertices.get(idVertex);
		Arco[] resp = new Arco[encontrado.darsizeListaAdja()];
		int cont= 0; 
		
		
		while(arcosConectadosaK(idVertex).hasNext())
		{
			resp[cont] = arcosConectadosaK(idVertex).next();
			cont ++;
		}
		
		
		return resp;
	}
	


	public void uncheck()
	{
		listaconVertices.desmarcarTodos();
	}


	@Override
	public Iterator<K> iterator() 
	{
		// TODO Auto-generated method stub
		return null;
	}
	







}
