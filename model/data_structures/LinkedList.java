package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements Iterable<T> 
{
    private NodeListaEnlazada<T> first;    
    private int n;               

    public static class NodeListaEnlazada<T> 
    {
        private T item;
        private NodeListaEnlazada<T> siguiente;
        
        public T getItem() {
        	return item;
        }
        
        
    }

    /**
     * crea la lista enlazada
     */
    public LinkedList() {
        first = null;
        n = 0;
    }

    /**
     * Retorna true si la lista esta vacia
     
     */
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * Retorna el numero de nodos de la lista 
     *
     * @return the number of items in this bag
     */
    public int size() {
        return n;
    }

    /**
     * Anade un nodo a la lista 
     *
     * @param  item the item to add to this bag
     */
    public void add(T item) {
        NodeListaEnlazada<T> oldfirst = first;
        first = new NodeListaEnlazada<T>();
        first.item = item;
        first.siguiente = oldfirst;
        n++;
    }


    /**
     * Retorna el iterator
     * @return an iterator that iterates over the items in this bag in arbitrary order
     */
    public Iterator<T> iterator()  {
        return new ListIterator(first);  
    }


    
    private class ListIterator implements Iterator<T> {
        private NodeListaEnlazada<T> current;

        public ListIterator(NodeListaEnlazada<T> first) {
            current = first;
        }

        public boolean hasNext()  
        { 
        	return current != null;                     
        }

        public T next() 
        {
            if (!hasNext()) throw new NoSuchElementException();
            T item = current.item;
            current = current.siguiente; 
            return item;
        }
    }
}



