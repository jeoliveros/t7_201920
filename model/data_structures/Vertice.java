package model.data_structures;

import java.util.Iterator;

public class Vertice <K extends Comparable<K>, V >
{
	
	
	K key;
	
	V vertice;
	
	
	private LinkedList<Arco> listaDeAdjacencia;
	
	private boolean marcado;
	
	public Vertice(K pkey, V pvertice )
	{
		
		this.key = pkey;
		this.vertice = pvertice;
		this.listaDeAdjacencia = new LinkedList<Arco>();
		marcado = false;
		
		
	}
	
	public int darsizeListaAdja()
	{
		return listaDeAdjacencia.size();
	}
	
	
	public void marcarOdesmarcar(boolean accion)
	{
		marcado = accion; 
	}
	
	
	public K dar_Key()
	{
		return key;
	}
	
	
	public V getVertice() 
	{
		return vertice;
	}
	
	public void setVertice(V pvertice)
	{
		vertice = pvertice;
	}

	
	public Iterator<Arco> darlistaAdjacencias()
	{
		return listaDeAdjacencia.iterator() ;
	}
	
	public int darnumArcos()
	{
		return listaDeAdjacencia.size();
	}
	
	
	public void AgregarAdjacenciaALista( Double pcost, Vertice verticeinicial , Vertice verticeFinal)
	{
		
		Arco arcoAgregar = new Arco(verticeinicial, verticeFinal, pcost);
		
		
		listaDeAdjacencia.add(arcoAgregar);	
	}

}
