package model.logic;

public class Interseccion 
{

	private static int IDNodo;
	
	private static double longitud;
	
	private static double latitud;
	
	private static int MOVEMENT_ID;
	
	private boolean yaPasoPorAqui;
	
	
	public Interseccion(int pidNodo, double pLongitud, double pLatitud, int pMOVEMENT_ID   )
	{
		
		IDNodo= pidNodo;
	
		longitud = pLongitud;
	
		latitud = pLatitud;
		
		MOVEMENT_ID = pMOVEMENT_ID;
		
	}


	public  int DarIDNodo() {
		return IDNodo;
	}


	public  void Asignar_IDNodo(int iDNodo) {
		IDNodo = iDNodo;
	}


	public  double DarLongitud() {
		return longitud;
	}


	public  void Asignar_Longitud(double longitud) {
		Interseccion.longitud = longitud;
	}


	public  double DarLatitud() {
		return latitud;
	}


	public  void Asignar_Latitud(double latitud) {
		Interseccion.latitud = latitud;
	}


	public  int DarMOVEMENT_ID() {
		return MOVEMENT_ID;
	}


	public  void Asignar_MOVEMENT_ID(int mOVEMENT_ID) {
		MOVEMENT_ID = mOVEMENT_ID;
	}
	
	
	

	
}
