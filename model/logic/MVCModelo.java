package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import model.data_structures.*;
import model.logic.*;
import model.logic.Maps;
/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{

	public final static String DATOS_MALLA_VERTICES = "./data/bogota_vertices.txt";

	public final static String DATOS_MALLA_ARCOS = "./data/bogota_arcos.txt";



	/**
	 * Atributos del modelo del mundo
	 */
	private GrafoNoDirigido<Integer, Interseccion> grafoNodirigido ;

	private GrafoNoDirigido grafoNodirigidoCargadoConJson ;

	private Maps mapa;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		grafoNodirigido = new GrafoNoDirigido<Integer, Interseccion>(225894);

		grafoNodirigidoCargadoConJson = new GrafoNoDirigido(225894);

		//mapa = new Maps(grafoNodirigido);

	}


	/**
	 * Servicio de consulta de numero de vertices y arcos 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darNumVertices(){
		return grafoNodirigido.DarNumeroDeVertices();
	}

	public int darNumArcos(){
		return grafoNodirigido.DarNumeroDeEdges();
	}

	public int darNumVerticesGrafoJson(){
		return grafoNodirigidoCargadoConJson.DarNumeroDeVertices();
	}

	public int darNumArcosGrafoJson(){
		return grafoNodirigidoCargadoConJson.DarNumeroDeEdges();
	}

	/**
	 * Este metodo carga en las estructuras de datos datos del archivo malla vial 
	 * @return
	 */
	public int cargarIntersecciones()
	{

		File archivo = new File(DATOS_MALLA_VERTICES);
		FileReader lector;
		int mallasviales=0;

		try {

			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);

			bf.readLine();
			String linea = bf.readLine();



			while(linea != null )
			{
				String[] datos = linea.split(";");



				int iDNodo = Integer.parseInt(datos[0]);
				double longitud = Double.parseDouble(datos[1]);
				double latitud = Double.parseDouble(datos[2]);
				int ID_MovementID = Integer.parseInt(datos[3]);


				Interseccion agregar = new Interseccion(iDNodo, longitud, latitud, ID_MovementID);

				grafoNodirigido.agregarVertice(iDNodo,agregar );


				linea = bf.readLine();
				mallasviales++;
			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		return mallasviales;
	}



	//Estas vías se representan como un conjunto de adyacencias donde el primer campo especifica el id del vértice origen, y los demás campos son los ids de sus vértices adyacentes:
	//0 1 733 
	//1 49
	public void  leerArcos()
	{

		File archivo = new File(DATOS_MALLA_ARCOS);
		FileReader lector;


		try {

			lector = new FileReader(archivo);
			BufferedReader bf = new BufferedReader(lector);

			String linea = bf.readLine();

			while(linea != null)
			{
				String[] datos = linea.split(" ");


				int iDNodo = Integer.parseInt(datos[0]);


				Interseccion verticeorigen = grafoNodirigido.getInfoVertex(iDNodo);

				int idOrigen = verticeorigen.DarIDNodo();

				double startLat = verticeorigen.DarLatitud();
				double startLong = verticeorigen.DarLongitud();


				for (int i = 1; i < datos.length; i++) 
				{
					int arcoConectado = Integer.parseInt(datos[i]);

					Interseccion verticedestino = grafoNodirigido.getInfoVertex(arcoConectado);

					int iddestino = verticedestino.DarIDNodo();
					double endLat = verticedestino.DarLatitud();
					double endLong = verticedestino.DarLongitud();

					double cost = Haversine.distance(startLat, startLong, endLat, endLong);


					grafoNodirigido.AgregarArco(idOrigen, iddestino, cost);




					System.out.println(arcoConectado);

					System.out.println(cost);



				}


				System.out.println("Leyendo Arcos ");
				linea = bf.readLine();

			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}

	}

	/**
	 * Este es el metodo que crea los archivos de Json
	 * @throws IOException
	 */
	public void crearElArchivoJson() throws IOException
	{

		File f = new File("./data/JsonGenerado.json");

		if(!f.exists())
			f.createNewFile();

		PrintWriter out = new PrintWriter(f);

		out.println("{");

		out.println("\"vertices\":[");

		for (int i = 0; i < grafoNodirigido.DarNumeroDeVertices(); i++) {

			Interseccion actual = grafoNodirigido.getInfoVertex(i);
			out.println("{");
			out.println("\"id\": " + actual.DarIDNodo() + ",");
			out.println("\"latitud\": " + actual.DarLatitud() + ",");
			out.println("\"longitud\": " + actual.DarLongitud() + ",");
			out.println("\"MOVEMENT_ID\": " + actual.DarMOVEMENT_ID() + ",");
			if(i == grafoNodirigido.DarNumeroDeVertices() - 1) {
				out.println("}");
			}
			else {
				out.println("},");
			}
		}

		out.println("],");

		out.println("\"arcos\":[");

		for (int i = 0; i < grafoNodirigido.DarNumeroDeVertices(); i++) {

			Iterator listaActual = ((JSONArray) grafoNodirigido.arcosConectadosaK(i)).iterator();
			out.println("{");
			out.println("\"idInicial\":"+ i);
			out.println("\"adyacentes\": [");
			while(listaActual.hasNext()) {
				NodeListaEnlazada actual = (NodeListaEnlazada) listaActual.next();//Puede fallar(?)
				out.println("{");
				out.println("\"idFinal\":" + actual.getItem() + ",");
				out.println("\"idFinal\":" + actual.getItem());//Ojo podria ser más de dos id finales corregir esto

				if(!listaActual.hasNext())
					out.println("}");
				else
					out.println("},");

			}
			out.println("]");
			if (i+1 == grafoNodirigido.DarNumeroDeVertices()) {
				out.println("}");
			}
			else{
				out.println("},");
			}
		}

		out.println("]");

		out.println("}");
	}


	/**
	 * Este metodo se va a encargar de Cargar los archivo Json
	 */
	public void CargaElArchivoJson()
	{
		try {


			//Leemos el archivo
			FileReader archivo = new FileReader("./data/JsonGenerado.json");
			BufferedReader lector = new BufferedReader(archivo);


			JsonElement elementoPrincipal = new JsonParser().parse(lector);

			//Cogemos toda la info del archivo JsonGenerado
			JsonObject esquinasMallaVial = elementoPrincipal.getAsJsonObject();

			//obtenemos cada vertice
			//Recuerda que cada vertice tiene un id y un arreglo
			JsonArray vertices = esquinasMallaVial.getAsJsonArray("vertices");//Revisar si se debe escribir aqui


			//Buscamos la informacion de cada vertice  
			for (int i = 0; i < vertices.size(); i++)
			{
				JsonObject SoloUnVertice = vertices.get(i).getAsJsonObject();


				if(SoloUnVertice == null)
					continue;


				int id = SoloUnVertice.get("id").getAsInt();

				if(id == 0)
					continue;

				double latitud = SoloUnVertice.get("latitud").getAsDouble();

				if(latitud == 0.0)
					continue;
				double longitud = SoloUnVertice.get("longitud").getAsDouble();
				if(longitud == 0.0)
					continue;
				int MOVEMENT_ID =SoloUnVertice.get("MOVEMENT_ID").getAsInt(); 
				// Guardamos los valores de interseccion y de aqui todo lo guardamos en el vertice
				Interseccion interseccion = new Interseccion(id,latitud, longitud, MOVEMENT_ID);

				grafoNodirigidoCargadoConJson.agregarVertice(id, interseccion);

			}
			//Lectura de los arcos que se encuentran en la lectura
			JsonArray interseccion = esquinasMallaVial.get("arcos").getAsJsonArray();
			
			//hacemos el recorrido de arcos
			//Recuerda que el arco contiene un id y una lista de id de otros vertices a los que conecta
			for (int i = 0; i < interseccion.size(); i++) {
				JsonObject SoloUnArco = interseccion.get(i).getAsJsonObject();

				int  id = SoloUnArco.get("idInicial").getAsInt();

				if(grafoNodirigidoCargadoConJson.getInfoVertex(id)!=null) {
					Interseccion infoInterseccion=  (Interseccion) grafoNodirigidoCargadoConJson.getInfoVertex(id);

					JsonArray arregloDeVertices= SoloUnArco.get("adyacentes").getAsJsonArray();
					for (int j = 0; j < arregloDeVertices.size(); j++) {
						int unVertice= arregloDeVertices.get(i).getAsInt();
						Interseccion infoVertices = (Interseccion) grafoNodirigidoCargadoConJson.getInfoVertex(unVertice); 

						if(grafoNodirigidoCargadoConJson.getInfoVertex(unVertice)!=null) {


							Interseccion Infointerseccion2=  (Interseccion) grafoNodirigidoCargadoConJson.getInfoVertex(unVertice);
							double costo= Haversine.distance(infoInterseccion.DarLatitud(), infoInterseccion.DarLongitud(), Infointerseccion2.DarLatitud(), Infointerseccion2.DarLongitud());
							grafoNodirigidoCargadoConJson.AgregarArco(infoInterseccion.DarIDNodo(), Infointerseccion2.DarIDNodo(), costo);
						}
					}
				}
			}
		}
		catch (IOException e) {

			e.printStackTrace();
		}

	}




	public int darNumComponentesConectadas()
	{
		Connected_Components resp = new Connected_Components(grafoNodirigido);

		int numcomp = resp.count();

		return numcomp;
	}

	public void graficar()
	{
		mapa.initFrame("hola mundo");
	}


}
