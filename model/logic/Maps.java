
  
package model.logic;
import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.Polygon;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.*;

import model.logic.MVCModelo;
import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

/**
 * Esta clase representa los objetos y metodos basicos
 * necesarios para la realizacion de talleres y proyectos
 * que tengan que ver con dibujo de mapas
 * @author Juan Pablo Cano
 */
public class Maps extends MapView
{
    //-------------------------
    // Atributos
    //------------------------

    /**
     * Mapa cargado de JxMaps
     */
    private Map map;
    
    
    

    /**
     * Tabla de Hash que contiene las universidades en llaves
     * y las Latitudes y Longitudes en Valor
     */
    private HashTable<Integer, LatLng> table;

    
    
    
    /**
     * Arreglo de latitudes y longitudes
     */
    private LatLng[] locs;
    
    //-----------------------
    // Constructores
    //------------------------

    /**
     *
     */
    public Maps(GrafoNoDirigido grafo)
    {
    	
        table = new HashTable<>();
        locs = new LatLng[12];
        
        try
        {
            // El archivo de locaciones

        	Iterator<Interseccion>  iter=  grafo.iterator();
        	
            int i = 0;
            
            while(iter.hasNext())
            {

            	Interseccion inter = iter.next();
                
                
                double lat = inter.DarLatitud();
                double lon = inter.DarLongitud();
                int nomId = inter.DarMOVEMENT_ID() ;
                
                LatLng latLng = new LatLng(lat, lon);
                
                table.put(nomId, latLng);
                locs[i] = latLng;
           
                i++;
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

        // Se espera que JxMaps cargue el mapa
        setOnMapReadyHandler(new MapReadyHandler() {
            @Override
            public void onMapReady(MapStatus status)
            {
                if(status == MapStatus.MAP_STATUS_OK)
                {
                    map = getMap();
                    initMap(map);
                    /*
                     * Puede cambiar la funcion por otra
                     * Por ejemplo drawPolygon(Map map)
                     */
                    drawCircle(map);
                }
            }
        });
    }

    
    /**
     * Metodo que se encarga de dibujar un marcador de posicion
     * @param map Lienzo donde se colocar� el marcador
     */
    public void drawMarker(Map map)
    {
        for(int universidad: table)
        {
            LatLng loc = table.get(universidad);
            Marker marker = new Marker(map);
            marker.setPosition(loc);

            InfoWindow infoWindow = new InfoWindow(map);
            infoWindow.setContent("Hola "+ universidad);
            infoWindow.open(map, marker);
        }
    }

    /**
     * Metodo que se encarga de dibujar una linea con
     * el comportamiento de un poligono
     * @param map Lienzo donde se colocar�n las lineas
     */
    public void drawPolygon(Map map)
    {
        PolygonOptions po = new PolygonOptions();
        // Color de relleno
        po.setFillColor("#FF0000");
        // Opacidad de relleno
        po.setFillOpacity(0.35);

        // Los dos extremos del poligono
        LatLng[] path = new LatLng[2];
        int i = 0;

        for(int universidad : table)
        {
            path[0] = locs[i];
            path[1] = locs[i+1];
            // Objeto de latitud y longitud
            LatLng loc = table.get(universidad);
            // Marcador
            Marker marker = new Marker(map);
            marker.setPosition(loc);

            // Ventana de informacion
            InfoWindow infoWindow = new InfoWindow(map);
            infoWindow.setContent("Hola "+ universidad);
            infoWindow.open(map, marker);

            // Poligono
            Polygon polygon = new Polygon(map);
            // Se le agrega el camino, debe ser un arreglo de LatLng
            polygon.setPath(path);
            // Se colocan las opciones
            polygon.setOptions(po);
            i++;
        }
    }

    /**
     * M�todo que se encarga de dibujar los circulos
     * @param map Lienzo donde se colocar�n los circulos
     */
    public void drawCircle(Map map)
    {
        // Las opciones de creaci�n del c�rculo
        CircleOptions co = new CircleOptions();
        // Opacidad de relleno
        co.setFillOpacity(0.35);
        // Ancho de l�nea
        co.setStrokeWeight(1);
        // Opacidad de linea
        co.setStrokeOpacity(0.2);
        // Color de linea
        co.setStrokeColor("00FF00");

        for(int universidad: table)
        {
            // Objeto de latitud y longitud
            LatLng loc = table.get(universidad);

            // Creaci�n del c�rculo
            Circle circle = new Circle(map);
            // Centro
            circle.setCenter(loc);
            // Opciones
            circle.setOptions(co);
            // Radio
            circle.setRadius(10);
        }
    }

    /**
     * Inicialia el mapa de JxMaps
     * @param map El mapa que ser� inicializado
     */
    public void initMap(Map map)
    {
        // Las opciones del mapa
        MapOptions mapOptions = new MapOptions();
        // Los Controles del mapa
        MapTypeControlOptions controlOptions = new MapTypeControlOptions();
        mapOptions.setMapTypeControlOptions(controlOptions);

        // Centro del mapa
        map.setCenter(new LatLng(4.609537, -74.078715));
        // Zoom del Mapa
        map.setZoom(11.0);
    }

    /**
     * Inicializa el marco donde el mapa se va a cargar
     * @param titulo El t�tulo del marco
     */
    public void initFrame(String titulo)
    {
        JFrame frame = new JFrame(titulo);
        frame.setSize(700, 500);
        frame.setVisible(true);
        frame.add(this, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    
//    /**
//     * Main method
//     * @param args Parametros no necesarios
//     */
//    public static void main(String[] args)
//    {
//        Maps maps = new Maps();
//        maps.initFrame("Hola mundo");
//    }
}
