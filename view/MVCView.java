package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Crear Grafo con los datos de mallas viales de Bogota");
			System.out.println("2. Crear esquema JSON para persistir el grafo");
			System.out.println("3. cargar el grafo persistido en el paso 2");
			System.out.println("4. Cantidad de componentes conectados en el grafo construido");
			System.out.println("5. Grafique con ayuda de Google Maps la parte del grafo resultante");
			System.out.println("6. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			System.out.println(modelo);
		}
}
